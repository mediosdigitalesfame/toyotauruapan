<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Financiamiento</title>
    <?php include('contenido/head.php'); ?>  

</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
         <?php include('contenido/header.php'); ?>
         <?php include('contenido/analytics.php'); ?>

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">         

				<div class="container">
					<h2>Financiamiento Toyota®</h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
					<div class="row">
				</div>
			</div>



<!--barra información limpia-->
		<div class="section">
			<div id="about-section">

				<div class="welcome-box">
					<div class="container">
						<h1><span>La financiera de la familia</span></h1><br>
                        <h2>Accesible</h2>
 <p align="justify">
- Por medio de Toyota Financial Services México (TFSM), tú podrás estrenar el vehículo Toyota de tu agrado, ya que cuenta con atractivos planes de financiamiento, los cuales fueron creados pensando en tus necesidades.<br>
<br>
- Con TFSM tú puedes personalizar tu crédito e incluso participar en atractivas promociones si cuentas con un buen historial crediticio.<br><br>

- El equipo de TFSM te ofrece la confianza de que tus datos personales se manejan de forma confidencial, siendo resguardados por la familia Toyota.<br><br>

- Para tu seguridad y tranquilidad, te ofrecemos diferentes tipos de protección, como seguro de daños para el vehículo con las mejores coberturas, seguro de vida y desempleo, seguro de accidentes personales, responsabilidad civil en Estados Unidos y protección extra para tu vehículo.<br><br>

- Como cliente de TFSM tú podrás tener acceso a servicios en línea como consulta de tu estado de cuenta, realizar el pago de tu mensualidad, simular un pago anticipado para ahorrar intereses o recibir atención de nuestra área de servicio.<br><br>

- Para mayor información o si deseas preautorizar tu crédito en este momento, visita la página: <a href="http://www.toyotacredito.com.mx/" target="_blank"><strong>www.toyotacredito.com.mx</strong></a><br>
</p><br><br>

<br><br>


                	</div>
				</div>
            </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('contenido/footer.php'); ?>

</body>
</html>