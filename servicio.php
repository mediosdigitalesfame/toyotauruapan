<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Cita de Servicio</title>
    <?php include('contenido/head.php'); ?>

</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    <?php include('contenido/header.php'); ?>
    <?php include('contenido/analytics.php'); ?>
         
    
		
			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Agenda tu cita de Servicio</h2>

				</div>
			</div>
			
				 <div class="about-box">
				 <div class="container">
				 	 <div class="col-md-3" align="center">
					 </div>     	 
                     <div class="col-md-6" align="center">
							 <?php include('form.php'); ?>
					 </div>     	 
			     </div>
			 </div>



			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">

						<div class="col-md-12" align="center">
						  <h3>Cita de Servicio</h3>
                            

    
                                
<p align="justify">*Le recordamos que las citas de manejo y servicio están sujetas a horarios disponibles.</p><br><br><br>
                            

						</div>

					</div>
                    
			<h2><span>La satisfacción del cliente es nuestra prioridad</span></h2><br>
						<p align="justify"><strong>Calidad y servicio</strong><br><br>

Nuestra mayor preocupación es la seguridad de nuestros clientes, es por ello que ponemos especial atención en el buen funcionamiento de tu automóvil. Día con día nos esforzamos por ofrecer el mejor servicio y atención, así como el equipo especializado y la tecnología de punta para mantener tu vehículo. Nuestro compromiso es continuar ofreciendo mayor valor y calidad desde el momento en que compras tu automóvil.</p><br>
                 
<p align="justify"><h2>Nuestro equipo de expertos</h2>
<p><strong>Especialización</strong><br><br>
El personal del área de servicio, como son los asesores y el equipo técnico de cada uno de los Distribuidores cuenta con los recursos técnicos y conocimientos necesarios para atender tus necesidades y mantener en buen estado cada uno de los vehículos Toyota.<br><br>

Nuestros expertos técnicos son capacitados en el Centro de Entrenamiento Toyota, en donde nos aseguramos que cada uno de ellos cuente con el conocimiento necesario para solucionar cualquier situación que presente tu automóvil. Las especializaciones que obtienen nuestros técnicos son: <strong>Mantenimiento, Reparación y Maestro en Diagnóstico, entre otras.</strong></p><br><br>


<p align="justify"><h2>Equipo y herramental Toyota</h2>
<p><strong>Vanguardia</strong><br><br>
Puedes tener la tranquilidad y seguridad de que el mantenimiento de tu automóvil Toyota es realizado con equipo y herramental especializado, el cual se requiere para llevar a cabo las revisiones que sean necesarias o bien, alguna reparación. Todas las agencias Toyota están equipadas con la más alta tecnología, para garantizar el buen estado de tu automóvil, tu seguridad y satisfacción al volante.
</p><br><br>


<div class="single-project-content">
                            <div class="container">
							 
							<div class="col-md-10"><img alt="" src="images/servicio.jpg"></div>
                         	</div>
                            </div>
                                                        
						<div class="single-project-content">
                        <div class="container">
                         
						<div class="col-md-10"><img alt="" src="images/aceite.jpg"></div>
                         </div>  
                         </div>     
                            
						<div class="single-project-content">
                        <div class="container">
                         
						<div class="col-md-10">	<img alt="" src="images/frenos.jpg"></div>
                         	</div> </div>                                                                     
                    
				</div>
			</div>

		</div>
        
		<!-- End content -->
	
	<!-- End Container -->
    
			<?php include('contenido/footer.php'); ?>

	
</body>
</html>